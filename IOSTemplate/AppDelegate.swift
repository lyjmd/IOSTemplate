//
//  AppDelegate.swift
//  eval
//
//  Created by hengchengfei on 15/8/19.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit
import XCGLogger
import IOSCommon

let log = XCGLogger.defaultInstance()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
 
    let tabBarController: CFSTabBarController = {
        let tabbar:CFSTabBarController = CFSStoryboardTabbar("CFSTabBarController")
        let nav1:UINavigationController = CFSStoryboardHome("CFSHomeNav")
        
        let nav2:UINavigationController = CFSStoryboardRank("CFSRankNav")
        let nav3:UINavigationController = CFSStoryboardUser("CFSUserNav")
        
        tabbar.viewControllers = [nav1,nav2,nav3]
        
        return tabbar
    }()
    
    // MARK: - UIApplicationDelegate
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = self.tabBarController
        self.tabBarController.selectedIndex = 0
        
        setupApperance()
        setupLog()
        
        self.window!.makeKeyAndVisible()
        return true
    }
     
    /**
      属性设置
     */
    func setupApperance(){
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        // 设置导航栏的背景,文字颜色
        UINavigationBar.appearance().barTintColor = UIColor(netHex: 0x00AAEE, alpha: 1.0) //导航栏背景色
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]

        // 设置导航栏的title为空
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics: .Default)
        
    }
    
    /// 配置Log级别
    func setupLog(){
        if isPrintLog {
            log.setup(.Debug, showLogIdentifier: false, showFunctionName: false, showThreadName: false, showLogLevel: true, showFileNames: true, showLineNumbers: true, showDate: true, writeToFile: nil, fileLogLevel: nil)
        }else{
            log.setup(.Severe, showLogIdentifier: false, showFunctionName: false, showThreadName: false, showLogLevel: true, showFileNames: true, showLineNumbers: true, showDate: true, writeToFile: nil, fileLogLevel: nil)
        }
        
        
    }
    
}

