//
//  CFSBaseModel.swift
//  eval
//
//  Created by hengchengfei on 15/8/21.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit
import ObjectMapper

public class BaseModel<T:Mappable>: Mappable {
    
    public var code:String?
    public var data:[T]?
    
    public init(){
        
    }
    
    required public init?(_ map: Map) {
        
    }
    
    public func mapping(map: Map) {
        code <- map["code"]
        data <- map["data"]
    }
}
