


//
//  String.swift
//  eval
//
//  Created by hengchengfei on 15/9/4.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit

public extension String {
    func sizeWithFont(font:UIFont!,constrainedToWidth width: CGFloat) -> CGSize {
        return NSString(string: self).boundingRectWithSize(CGSize(width: width, height: 9999), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName:font], context: nil).size
    }
}
